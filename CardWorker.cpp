#include "CardWorker.h"




void CardWorker::ParseCards(TQueue<Patient>* pq)
{
	std::string buff;
	Patient tmp;
	std::string cardpath = Path + "\\1.txt";
	std::ifstream PatientDB;
	int i = 1;

	PatientDB.open(cardpath, std::fstream::in);
	while(PatientDB.is_open()) {
		std::getline(PatientDB, buff);
		tmp = StringToPatient(buff);
		pq->Push(tmp);
		
		
		PatientDB.close(); 

		cardpath = Path + "\\" + std::to_string(++i) + ".txt";
		PatientDB.open(cardpath, std::fstream::in);	
	}
}
