#pragma once
#include "CardWorker.h"
#include "TDate.h"


class PatientsController {
private:


	TQueue<Patient> PatientsQueue;
public:
	PatientsController();
	Patient GetNextPatientPatient();
	std::vector<std::string> GetNextPatient();
	std::string GetNextPatientString();
};



