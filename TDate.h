#pragma once
#include <string>

static bool isLeap(int year)
{
    return (((year % 4 == 0) &&
        (year % 100 != 0)) ||
        (year % 400 == 0));
}

static int DateStringToInt(std::string date, std::string splitby) { // DDMMYYYY
    date.erase(remove(date.begin(), date.end(), splitby[0]), date.end());
    return stoi(date);
}

//std::string(number_of_zeros, '0').append(original_string);

static std::string DateToString(int Dob) {
    std::string tmp, buff;
    buff = std::to_string(Dob / 1000000);
    tmp += std::string(2 - buff.size(), '0').append(buff) + '/';
    buff = std::to_string(Dob / 10000 % 100);
    tmp += std::string(2 - buff.size(), '0').append(buff) + '/';
    buff = std::to_string(Dob % 10000);
    tmp += std::string(4 - buff.size(), '0').append(buff);
    return tmp;
}

static bool CheckDate(int date) { // DDMMYYYY
    int y = date % 10000;
    int m = (date / 10000) % 100;
    int d = date / 1000000;
    if (y > 3000 ||
        y < 1100)
        return false;
    if (m < 1 || m > 12)
        return false;
    if (d < 1 || d > 31)
        return false;

    if (m == 2)
    {
        if (isLeap(y))
            return (d <= 29);
        else
            return (d <= 28);
    }

    if (m == 4 || m == 6 ||
        m == 9 || m == 11)
        return (d <= 30);

    return true;
}