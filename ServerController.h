#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "PatientsController.h"
#include <boost/asio.hpp>
#include <boost/bind.hpp>


class ServerController
{
private:
	
	bool doctor_flag = false;
	
	PatientsController PC;
	void Run();
	std::string ParseResponse(std::string msg);
	
public:
	ServerController();
};


