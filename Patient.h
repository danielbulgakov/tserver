#pragma once

#include <string>
#include <vector>
#include "TDate.h"

struct Patient {
	std::string FirstName = "", MiddleName = "", LastName = "";
	uint32_t Dob = 0; // DDMMYYYY
	std::string Gender;

	bool isValid() {
		return FirstName != std::string();
	}
};

static Patient StringToPatient(std::string data)
{
	Patient tmp;
	if (data.length() < 20) return tmp; // PEREDELAT


	std::vector<std::string> pt;
	const std::string splitby = " ";
	const std::string splitdate = "/";
	size_t pos = 0;

	while ((pos = data.find(splitby)) != std::string::npos) {
		pt.push_back(data.substr(0, pos));
		data.erase(0, pos + splitby.length());
	}

	if (!CheckDate(DateStringToInt(pt[3], splitdate))) {
		return tmp;
	}
	tmp.FirstName = pt[0];
	tmp.MiddleName = pt[1];
	tmp.LastName = pt[2];
	tmp.Dob = DateStringToInt(pt[3], splitdate);
	tmp.Gender = data;

	return tmp;
}







