#pragma once

#include "Patient.h"
#include "TQueue.h"
#include <vector>
#include <string>
#include <fstream>
#include <chrono>


class CardWorker {
private:

	std::string Path = "Patients";
	

public:

	void ParseCards(TQueue<Patient>* pq);

};

