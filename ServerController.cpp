#include "ServerController.h"

void ServerController::Run()
{
	boost::asio::io_service ios;

	boost::asio::streambuf buff;
	std::ostringstream copy;

	boost::asio::ip::tcp::endpoint ep = boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 8080);
	boost::asio::ip::tcp::acceptor acceptor(ios, ep);
	std::string msg("");
	

	while (true) {

		boost::asio::ip::tcp::socket sock(ios);
		acceptor.accept(sock);
		size_t bytes = read_until(sock, buff, '\n');
		copy.str("");
		copy << &buff;
		buff.consume(bytes);
		msg = copy.str();
		if (!msg.empty()) {
			msg.pop_back();
		}
		sock.write_some(boost::asio::buffer(ParseResponse(msg) + "\n"));

		sock.close();
	}
}

std::string ServerController::ParseResponse(std::string msg)
{
	if (msg == "Connect") {
		std::cout << "LOG | Connection Established\n";
		return "Connection Established";
	}
	if (msg == "Doctor") {
		doctor_flag = true;
		std::cout << "LOG | Doctor logined to the server!\n";
		return std::string("Doctor logined to the server");
	}
	if (msg == "Next" && doctor_flag) {
		std::string buff = PC.GetNextPatientString();
		std::cout << "LOG | " << buff << std::endl;
		return buff;
	}
	if (msg == "Quit") {
		exit(0);
	}
	
	return "Type -help to get usefull commands";
}





ServerController::ServerController()
{
	std::cout << "Server Logs" << std::endl;
	Run();
}
