#pragma once

#include "TStack.h"

template <class ValType>
class TQueue : protected TStack<ValType>
{
private:
	int32_t LowIndex;

	virtual int32_t GetNextIndex(size_t ind) {
		
		return ++ind % this->Num;
	}

public:
	TQueue<ValType>(size_t size = 2) : TStack<ValType>(size) {};

	virtual ValType Pop() {
		if (this->IsEmpty())
			throw std::runtime_error("Queue is Empty!");
		else {
			ValType& Temp = this->Data[LowIndex];
			if (this->LowIndex == this->TopIndex) {
				this->LowIndex = this->TopIndex = -1;
			}
			else {
				LowIndex = GetNextIndex(LowIndex);
			}
			return Temp;
		}

	}

	virtual bool IsFull() {
		if (this->LowIndex == GetNextIndex(this->TopIndex) &&
			this->TopIndex != -1 &&
			this->LowIndex != -1
			) return true;
		
		return false;
		
	}

	virtual void Push(ValType elem) {
		if (this->IsFull()) {
			ValType* tmp = this->Data;
			this->Data = new ValType[this->Num + 10];
			int j = this->LowIndex;
			for (size_t i = 0; i < this->Num; i++) {
				this->Data[i] = tmp[j];
				j = GetNextIndex(j);
			}
			this->LowIndex = 0;
			this->TopIndex = this->Num - 1;
			this->Num += 10;
			delete[] tmp;
		}
		this->TopIndex = GetNextIndex(this->TopIndex);
		this->Data[this->TopIndex] = elem;
		if (this->IsEmpty()) this->LowIndex = 0;
	}

};