#include "PatientsController.h"

PatientsController::PatientsController()
{
	TQueue<Patient> p(2);
	PatientsQueue = p;
	CardWorker CW;
	CW.ParseCards(&PatientsQueue);
}

Patient PatientsController::GetNextPatientPatient()
{
	return PatientsQueue.Pop();
}

std::vector<std::string> PatientsController::GetNextPatient(){
	std::vector<std::string> tmp;
	try
	{
		Patient p = PatientsQueue.Pop();
		tmp.push_back(p.FirstName);
		tmp.push_back(p.MiddleName);
		tmp.push_back(p.LastName);
		tmp.push_back(DateToString(p.Dob));
		tmp.push_back(p.Gender + "");
	}
	catch (std::runtime_error& e)
	{
		tmp.push_back(e.what());
		return tmp;
	}



	return tmp;

}

std::string PatientsController::GetNextPatientString()
{
	std::string tmp = std::string();
	for (auto x : GetNextPatient()) {
		
		tmp += x + " ";
	}
	return tmp;
}

